import Vuex from "vuex";

const state = {
  user: null,
  showHeader: true,
};

const store = new Vuex.Store({
  state,
  getters: {
    user: (state) => {
      return state.user;
    },
    showHeader: (state) => {
      return state.showHeader;
    },
  },
  actions: {
    user(context, user) {
      context.commit("user", user);
    },
  },
  mutations: {
    user(state, user) {
      state.user = user;
    },
    swithHeader(state, swith) {
      state.showHeader = swith;
    },
  },
});

export default store;
