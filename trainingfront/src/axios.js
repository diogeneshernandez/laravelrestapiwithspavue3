import axios from "axios";

axios.defaults.baseURL = "http://api.training.local:8000/";
axios.defaults.withCredentials = true;
if (localStorage.getItem("token")) {
  axios.defaults.headers.common["content-type"] = "application/json";
  axios.defaults.headers.common["authorization"] =
    "Bearer " + localStorage.getItem("token");
}
