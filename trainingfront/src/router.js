import { createWebHistory, createRouter } from "vue-router";

//Components
import Dashboard from "./components/Pages/Dashboard.vue";
import Login from "./components/Pages/Login.vue";
import Register from "./components/Pages/Register.vue";
import Exercises from "./components/Pages/Exercises.vue";

const routes = [
  { path: "/login", component: Login, name: "Login" },
  { path: "/register", component: Register, name: "Register" },
  {
    path: "/",
    component: Dashboard,
    name: "Dashboard",
    meta: { requiresAuth: true },
  },
  {
    path: "/exercises",
    component: Exercises,
    name: "Exercises",
    meta: { requiresAuth: true },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (localStorage.getItem("token")) {
      next({ name: "Login" });
    } else {
      next(); // go to wherever I'm going
    }
  } else {
    next(); // does not require auth, make sure to always call next()!
  }
});

export default router;
