module.exports = {
  content: ["./public/**/*.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: [
      {
        training: {
          primary: "#3bbddb",
          secondary: "#d86649",
          accent: "#5fd85d",
          neutral: "#221E29",
          "base-100": "#213845",
          info: "#92B0ED",
          success: "#85E0A6",
          warning: "#E8A811",
          error: "#DD2238",
        },
      },
    ],
  },
  plugins: [require("daisyui")],
};
