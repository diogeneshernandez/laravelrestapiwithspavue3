## Projeto Laravel 9 Rest API + SPA VueJS 3

### Intuito do projeto
- Criar uma aplicação Rest API e Front desacoplado com uma SPA VueJs 3 para cadastro e controle de exercicios de academia.
- Vincular exercicios a aparelhos
- Autenticação via token e sessão através dos recursos do Laravel Sanctum
- Versatilidade para consumo da API por aplicação mobile com o uso da emissão de token
- Login, cadastro e cruds.

### TODOS
1. Melhorar metodos de autenticação para a session/csrf cookie do sanctum
2. Implantar crud para aparelhos e exercicios
3. Criar relação entre os items acima
4. Coverage 100% do projeto