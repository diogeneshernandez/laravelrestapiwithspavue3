<?php

namespace App\Application\Api\User\Controllers;

use Illuminate\Support\Facades\Auth;

class UserController
{
    public function index()
    {
        return Auth::user();
    }
}
