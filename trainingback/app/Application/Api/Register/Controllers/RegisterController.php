<?php

namespace App\Application\Api\Register\Controllers;

use App\Application\Api\Register\Requests\RegisterRequest;
use App\Domain\Register\Actions\RegisterAction;
use App\Domain\Register\DataTransferObjects\RegisterData;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function register(RegisterRequest $registerRequest)
    {
        $data = RegisterData::fromRequest($registerRequest);

        $register = (new RegisterAction)($data);

        if ($register) {
            return response(
                [
                    'message' => "Usuário cadastrado com sucesso",
                    'user' => $register
                ],
                201
            );
        }

        return response(
            [
                'message' => "Ocorreu um erro ao registrar o usuário. Verifique os dados e tente novamente."
            ],
            500
        );
    }
}
