<?php

namespace App\Application\Api\Register\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'lastname' => ['required'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => [
                'required',
                'min:6',
                'confirmed'
            ],
            'agree' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Você precisa preencher o seu nome.',
            'lastname.required' => 'Você precisa preencher o seu sobrenome',
            'email.required' => 'Você precisa preencher o seu e-mail para poder logar no sistema.',
            'password.required' => 'Você precisa digitar uma senha para poder logar no sistema.',
            'email.email' => 'Seu e-mail precisa atender o formato padrão de e-mail com abc@abc.com.br',
            'password.min' => 'O tamanho minimo da senha deve ser maior que 6 caracteres',
            'password.confirmed' => 'Sua confirmação de senha não é igual a senha digitada',
            'agree.required' => 'Você precisa aceitar os termos para se registrar em nossa plataforma'
        ];
    }
}
