<?php

namespace App\Application\Api\Exercise\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreExercisesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'unique:exercises,name'],
            'description' => ['required'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'É obrigatório que o exercicio tenha um nome.',
            'description.required' => 'É obrigatório inserir uma breve descrição do exercicio.',
            'name.unique' => 'O nome não pode se repetir com um exercicio ja cadastrado na plataforma.',
        ];
    }
}
