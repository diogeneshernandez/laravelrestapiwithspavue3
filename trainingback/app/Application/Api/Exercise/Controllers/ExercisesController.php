<?php

namespace App\Application\Api\Exercise\Controllers;

use App\Application\Api\Exercise\Requests\StoreExercisesRequest;
use App\Application\Api\Exercise\Resources\ExercisesCollection;
use App\Domain\Exercise\Actions\StoreExercisesAction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Exercise\Models\Exercise;
use App\Domain\Exercise\DataTransferObjects\StoreExercisesData;

class ExercisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Exercise $exercise)
    {
        return new ExercisesCollection($exercise->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Application\Api\Exercise\Requests\StoreExercisesRequest $storeExercisesRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExercisesRequest $storeExercisesRequest)
    {
        $data = StoreExercisesData::fromRequest($storeExercisesRequest);

        $stored = (new StoreExercisesAction)($data);

        if ($stored) {
            return response([
                'success' => true,
                'message' => 'Exercicio armazenado com sucesso!',
                'exercise' => $stored,
            ]);
        }

        return response([
            'success' => false,
            'message' => 'Ocorreu um problema ao armazenar seu exercicio. Verifique e tente novamente.',
            'error' => $stored->getMessage(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
