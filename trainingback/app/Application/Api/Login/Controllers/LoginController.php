<?php

namespace App\Application\Api\Login\Controllers;

use App\Application\Api\Login\Requests\LoginRequest;
use App\Domain\Login\Actions\LoginAction;
use App\Domain\Login\DataTransferObjects\LoginData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

class LoginController extends Controller
{
    public function login(LoginRequest $loginRequest)
    {
        $data = LoginData::fromRequest($loginRequest);

        $login = (new LoginAction)($data);

        if ($login) {
            return response(
                [
                    'message' => 'success',
                    'token' => Arr::get($login, 'token'),
                    'user' => Arr::get($login, 'user'),
                ]
            );
        }

        return response(['message' => 'error', 'text' => 'Não foi possível realizar o seu login com os dados inseridos. Verique-os e tente novamente.']);
    }
}
