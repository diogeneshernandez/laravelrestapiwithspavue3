<?php

namespace App\Domain\Login\DataTransferObjects;

use App\Application\Api\Login\Requests\LoginRequest;
use Spatie\DataTransferObject\DataTransferObject;

class LoginData extends DataTransferObject
{
    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var string */
    public $deviceName;

    public static function fromRequest(LoginRequest $request): self
    {
        return new self([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'deviceName' => $request->get('device_name'),
        ]);
    }
}
