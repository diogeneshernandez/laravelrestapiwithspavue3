<?php

namespace App\Domain\Login\Actions;

use App\Domain\Login\DataTransferObjects\LoginData;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class LoginAction
{
    public function __invoke(LoginData $loginData)
    {
        $data = $loginData->all();

        $user = User::where('email', Arr::get($data, 'email'))->first();

        if (!$user || !Hash::check(Arr::get($data, 'password'), Arr::get($user, 'password'))) {
            return false;
        }

        return [
            'token' => $user->createToken(Arr::get($data, 'deviceName'))->plainTextToken,
            'user' => $user,
        ];
    }
}
