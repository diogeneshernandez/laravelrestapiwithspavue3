<?php

namespace App\Domain\Register\Actions;

use App\Domain\Register\DataTransferObjects\RegisterData;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class RegisterAction
{
    public function __invoke(RegisterData $registerData)
    {
        try {

            $data = $registerData->all();

            $user = User::create([
                "name" => Arr::get($data, 'name'),
                "email" => Arr::get($data, 'email'),
                "password" => Hash::make(Arr::get($data, 'password')),
            ]);

            if ($user) {
                //TODO:Create profile
                return $user;
            }
            return false;
        } catch (\Exception $e) {
            info($e->getMessage());
            return false;
        }
    }
}
