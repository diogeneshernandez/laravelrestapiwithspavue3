<?php

namespace App\Domain\Register\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;
use App\Application\Api\Register\Requests\RegisterRequest;

class RegisterData extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var string */
    public $lastname;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var bool */
    public $agree;

    public static function fromRequest(RegisterRequest $request): self
    {
        return new self([
            'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'agree' => $request->get('agree'),
        ]);
    }
}
