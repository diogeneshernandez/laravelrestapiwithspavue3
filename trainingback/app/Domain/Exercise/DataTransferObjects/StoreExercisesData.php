<?php

namespace App\Domain\Exercise\DataTransferObjects;

use App\Application\Api\Exercise\Requests\StoreExercisesRequest;
use Spatie\DataTransferObject\DataTransferObject;

class StoreExercisesData extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var string */
    public $description;

    public static function fromRequest(StoreExercisesRequest $request): self
    {
        return new self([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
        ]);
    }
}
