<?php

namespace App\Domain\Exercise\Actions;

use App\Domain\Exercise\Models\Exercise;
use App\Domain\Exercise\DataTransferObjects\StoreExercisesData;
use Exception;

class StoreExercisesAction
{
    public function __invoke(StoreExercisesData $storeExercisesData)
    {
        try {
            $data = $storeExercisesData->all();
            $exercise = Exercise::create($data);
            if ($exercise) {
                return $exercise;
            }
            return throw new Exception("Erro ao cadastrar exercicio com os dados informados.");
        } catch (\Exception $e) {
            return $e;
        }
    }
}
